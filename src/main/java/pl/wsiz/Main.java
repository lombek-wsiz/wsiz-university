package pl.wsiz;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello, World!");

        User user1 = new User("Adam", "Nowak", "adam@onet.eu",
                "adam@34", LocalDate.of(1992, 5, 20));

        User user2 = new User("Jan", "Kowalski", "janek@gmail.com",
                "jann4#1@34", LocalDate.of(2002, 2, 12));

        User user3 = new User("Edyta", "Nowak", "edyta.nowak@gmail.com",
                "edyta#%551", LocalDate.of(1990, 1, 3));

        FileUserRepository fileUserRepository = new FileUserRepository();
        fileUserRepository.insert(user1);

        List<User> users = new LinkedList<>();
        users.add(user1);
        users.add(user2);
        users.add(user3);

        System.out.println("Liczba użytkowników: "+users.size());
        for (User user: users) {
            System.out.println("=======");
            printUser(user);
        }
    }

    private static void printUser(User user) {
        System.out.println("Imię i nazwisko: "+user.getFirstName()+" "+user.getLastName());
        System.out.println("Adres email: "+user.getEmail());

        String europeanDatePattern = "dd.MM.yyyy";
        DateTimeFormatter europeanDateFormatter = DateTimeFormatter.ofPattern(europeanDatePattern);
        System.out.println("Data urodzenia: "+user.getDateOfBirth().format(europeanDateFormatter));
    }

}